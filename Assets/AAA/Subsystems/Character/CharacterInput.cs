using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Starformers
{
	public class CharacterInput : MonoBehaviour
	{
		public bool JumpInput;
		public bool DashInput;
		public float HorizontalInput;
		
		private void Update()
		{
			//Input
			HorizontalInput = Input.GetAxis("Horizontal");
			JumpInput = Input.GetKeyDown(KeyCode.Space);
			DashInput = Input.GetKeyDown(KeyCode.LeftShift);
		}
	}
}