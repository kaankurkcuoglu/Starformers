using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Starformers
{
	public class CharacterController : MonoBehaviour
	{
		[FoldoutGroup("Links")] public Rigidbody CharacterRigidbody;
		[FoldoutGroup("Links")] public CharacterInput CharacterInput;

		[FoldoutGroup("Floating Settings")] public float RideHeight;
		[FoldoutGroup("Floating Settings")] public float RideHeightRayLenght;
		[FoldoutGroup("Floating Settings")] public float RideSpringStrength;
		[FoldoutGroup("Floating Settings")] public float RideSpringDamper;


		[FoldoutGroup("Controller Settings")] public float CharacterVelocityMultiplier;
		[FoldoutGroup("Controller Settings")] public float JumpDuration;
		[FoldoutGroup("Controller Settings")] public float JumpForce;
		[FoldoutGroup("Controller Settings")] public float JumpAmount;
		[FoldoutGroup("Controller Settings")] public float GroundedThreshold;
		[FoldoutGroup("Controller Settings")] public float DashForce;
		[FoldoutGroup("Controller Settings")] public float DashDuration;
		[FoldoutGroup("Controller Settings")] public float DashCooldown;


		[FoldoutGroup("Slope Settings")] public float MaxClimbAngle;
		[FoldoutGroup("Slope Settings")] public float SlopeGravityMultiplier;

		public LayerMask WalkableLayers;


		private bool IsJumping;
		private bool IsGrounded;
		public bool IsDashing;

		private float JumpTimer;
		private float JumpCounter;
		private float NextAvailableDashTime;
		private float DashEndTime;

		private bool CanDash;


		private void FixedUpdate()
		{
			//FloatingCapsule
			RaycastHit hit;
			var rayDidHit = Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, RideHeightRayLenght + RideHeight, WalkableLayers);
			var surfaceAngle = Vector3.Angle(hit.normal, Vector3.up);

			if (IsJumping && CharacterRigidbody.velocity.y < 0)
			{
				IsJumping = false;
			}


			if (!IsJumping && rayDidHit)
			{
				Vector3 velocity = CharacterRigidbody.velocity;
				Vector3 rayDirection = transform.TransformDirection(Vector3.down);

				Vector3 otherVelocity = Vector3.zero;
				Rigidbody hitRigidbody = hit.rigidbody;

				bool isOtherRigidbodyNull = hitRigidbody == null;


				if (!isOtherRigidbodyNull)
				{
					otherVelocity = hitRigidbody.velocity;
				}

				float rayDirectionVelocity = Vector3.Dot(rayDirection, velocity);
				float otherDirectionVelocity = Vector3.Dot(rayDirection, otherVelocity);

				float relativeVelocity = rayDirectionVelocity - otherDirectionVelocity;

				float x = hit.distance - RideHeight;
				IsGrounded = hit.distance < GroundedThreshold;
				if (IsGrounded)
				{
					JumpCounter = 0;
				}

				if (Time.time > NextAvailableDashTime && IsGrounded)
				{
					CanDash = true;
				}

				float springForce = (x * RideSpringStrength) - (relativeVelocity * RideSpringDamper);

				CharacterRigidbody.AddForce(rayDirection * springForce);


				if (!isOtherRigidbodyNull)
				{
					hitRigidbody.AddForceAtPosition(rayDirection * -springForce, hit.point);
				}


				Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * hit.distance,
					Color.yellow);
			}
			else
			{
				IsGrounded = false;
			}
		}


		private void Update()
		{
			if (IsDashing)
			{
				if (Time.time > DashEndTime)
				{
					IsDashing = false;
				}

				return;
			}


			var horizontalMovement = CharacterInput.HorizontalInput * CharacterVelocityMultiplier;
			var desiredVelocityX = horizontalMovement;


			if (JumpCounter < JumpAmount && CharacterInput.JumpInput)
			{
				Jump();
			}

			CharacterRigidbody.velocity = new Vector3(desiredVelocityX, CharacterRigidbody.velocity.y, 0f);

			if (desiredVelocityX > 0)
			{
				transform.eulerAngles = new Vector3(0, 90, 0);
			}
			else
			{
				transform.eulerAngles = new Vector3(0, 270, 0);
			}

			if (CharacterInput.DashInput && CanDash)
			{
				Dash();
			}
		}


		private void Dash()
		{
			CharacterRigidbody.AddForce(transform.forward * DashForce, ForceMode.Impulse);
			NextAvailableDashTime = Time.time + DashCooldown;
			DashEndTime = Time.time + DashDuration;
			IsDashing = true;
			CanDash = false;
		}

		private void Jump()
		{
			CharacterRigidbody.velocity = new Vector3(CharacterRigidbody.velocity.x, 0, 0);
			IsJumping = true;
			CharacterRigidbody.AddForce(Vector3.up * JumpForce, ForceMode.VelocityChange);
			JumpCounter++;
		}
	}
}